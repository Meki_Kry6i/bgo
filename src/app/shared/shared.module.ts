import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonsModule} from '@progress/kendo-angular-buttons/dist/npm/buttons.module';
import {GridModule} from '@progress/kendo-angular-grid/dist/npm/grid.module';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    BrowserAnimationsModule, ButtonsModule, GridModule
  ],
  declarations: [
  ]
})
export class SharedModule { }
