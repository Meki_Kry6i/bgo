import {Component, AfterViewInit} from '@angular/core';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'app';

  ngAfterViewInit() {
    // viewChild is set after the view has been initialized
    $('.player').YTPlayer({
      containment: '#intro',
      showControls: false,
      quality: 'hd720',
      autoPlay: true,
      loop : true,
      mute: true,
      startAt: 0,
      opacity: 1
    });
  }

}
